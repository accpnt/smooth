use ndarray::{Array1, array};

pub fn exponential_moving_average(x: &Array1<f64>, alpha: f64, adjust: bool) -> Array1<f64> {
    let n = x.len();
    let mut y: Array1<f64> = Array1::zeros(n);

    let w : f64 = 1.0 - alpha;

    y[0] = x[0];

    for t in 1..n {
        if adjust == true {
            let atm1 = (1.0 - w.powf((t+1) as f64)) / alpha;
            let at = (1.0 - w.powf(t as f64)) / alpha;
            y[t] = (x[t] + (w * y[t-1] * at)) / atm1;     
        }
        else {
            y[t] = alpha * x[t] + w * y[t-1];
        }
    }

    return y;
}

pub fn alpha_from_span(span: u64) -> f64 {
    if span < 1 {
        panic!("invalid span input value (span < 1)");
    }

    let alpha = 2.0 / (span as f64 + 1.0);

    return alpha;
}

pub fn alpha_from_com(com: f64) -> f64 {
    if com < 0.0 {
        panic!("invalid com input value (com < 0)");
    }

    let alpha = 1.0 / (com + 1.0);

    return alpha;
}

// truth values from pandas.ewm() 
#[test]
fn ema_from_com_adjust_true() {
    let values = array![0.0, 1.0, 2.0, 3.0, 4.0];
    let truth = array![0.0, 0.75, 1.6153846153846154, 2.5500000000000003, 3.520661157024793];
    let result = exponential_moving_average(&values, alpha_from_com(0.5), true);
    assert_eq!(truth, result);
}

#[test]
fn ema_from_com_adjust_false() {
    let values = array![0.0, 1.0, 2.0, 3.0, 4.0];
    let truth = array![0.0, 0.6666666666666666, 1.5555555555555556, 2.5185185185185186, 3.506172839506173];
    let result = exponential_moving_average(&values, alpha_from_com(0.5), false);
    assert_eq!(truth, result);
}

#[test]
fn ema_from_span_28_adjust_true() {
    let values = array![4.0, 3.0, 2.0, 1.0, 0.0];
    let truth = array![4.0, 3.4821428571428577, 2.9524011899702503, 2.4108052775250224, 1.8573973563517945];
    let result = exponential_moving_average(&values, alpha_from_span(28), true);
    assert_eq!(truth, result);
}

#[test]
fn ema_from_span_28_adjust_false() {
    let values = array![4.0, 3.0, 2.0, 1.0, 0.0];
    let truth = array![4.0, 3.9310344827586206, 3.797859690844233, 3.604903850096355, 3.35628979146902];
    let result = exponential_moving_average(&values, alpha_from_span(28), false);
    assert_eq!(truth, result);
}

#[test]
fn ema_from_span_48_adjust_true() {
    let values = array![4.0, 3.0, 2.0, 1.0, 0.0];
    let truth = array![4.0, 3.4895833333333335, 2.972226240416604, 2.4479347433116385, 1.9167172624585433];
    let result = exponential_moving_average(&values, alpha_from_span(48), true);
    assert_eq!(truth, result);
}

#[test]
fn ema_from_span_48_adjust_false() {
    let values = array![4.0, 3.0, 2.0, 1.0, 0.0];
    let truth = array![4.0, 3.9591836734693877, 3.8792169929196167, 3.761697931984122, 3.6081592408827294];
    let result = exponential_moving_average(&values, alpha_from_span(48), false);
    assert_eq!(truth, result);
}
