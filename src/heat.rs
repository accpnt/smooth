use ndarray::{s, concatenate, Axis, Array1, array};

pub fn explicit_heat_smooth(x: &Array1<f64>, t_end: f64) -> Array1<f64> {
    let k: f64 = 0.1; // time spacing
    let mut t: f64 = 0.0; // time spacing

    let mut p: Array1<f64> = x.clone();

    while t < t_end {
        // solve the finite difference scheme for the next time-step        
        p = k * (&p.slice(s![2..]) + &p.slice(s![..p.len()-2])) + &p.slice(s![1..p.len()-1]) * (1.0-2.0*k);

        // add the fixed boundary conditions since the above solves the 
        // interior points only
        let p0 : Array1<f64> = array![p[0]];
        let pn : Array1<f64> = array![p[p.len()-1]];
        p = concatenate![Axis(0), p0, p];
        p = concatenate![Axis(0), p, pn];

        t += k;
    }

    return p;
}