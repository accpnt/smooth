use ndarray::{Array1, array};

pub fn simple_moving_average(x: &Array1<f64>, window: usize) -> Array1<f64> {
    let n = x.len();
    let mut y = Array1::from_elem(n, f64::NAN);

    if window > n {
        return y; // Handle the case where the window size is greater than the array length
    }

    let w = window as f64;
    let mut sum: f64 = 0.0;

    // Calculate the initial sum for the first window
    for t in 0..window {
        sum += x[t];
    }

    for t in 0..n {
        if t >= window {
            // Remove the oldest value from the sum and add the newest value
            sum = sum - x[t - window] + x[t];
            y[t] = sum / w;
        }
    }

    return y;
}

// truth values from pandas.rolling(window).mean() 
#[test]
fn sma_window_2() {
    let values = array![0.0, 1.0, 2.0, 3.0, 4.0];
    let truth = array![f64::NAN, 0.5, 1.5, 2.5, 3.5];
    let result = simple_moving_average(&values, 2);
    assert_eq!(truth, result);
}

#[test]
fn sma_window_4() {
    let values = array![0.0, 1.0, 2.0, 3.0, 4.0];
    let truth = array![f64::NAN, f64::NAN, f64::NAN, 1.5, 2.5];
    let result = simple_moving_average(&values, 4);
    assert_eq!(truth, result);
}
