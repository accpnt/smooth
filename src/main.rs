
use csv::ReaderBuilder;
use plotly::{Plot, Scatter};
use ndarray::{Array1, Ix1};
use std::error::Error;
use chrono::NaiveDate;
use serde::{de, Deserialize, Deserializer};

use std::time::{Duration, Instant};

mod ema;
use ema::{alpha_from_span, exponential_moving_average};
mod heat;
use heat::explicit_heat_smooth;
mod sma;
use sma::simple_moving_average;


#[derive(Deserialize)]
struct Record {
    #[serde(deserialize_with = "naive_date_time_from_str")]
    date: NaiveDate,
    value: f64
}

fn naive_date_time_from_str<'de, D>(deserializer: D) -> Result<NaiveDate, D::Error>
where
    D: Deserializer<'de>,
{
    let s: String = Deserialize::deserialize(deserializer)?;
    NaiveDate::parse_from_str(&s, "%Y-%m-%d").map_err(de::Error::custom)
}

fn main() -> Result<(), Box<dyn Error>> {
    let mut reader = ReaderBuilder::new()
        .has_headers(true)
        .delimiter(b',')
        .from_path("./data/copper-prices-historical-chart-data-usd-per-pound-macrotrends.csv")?;
    
    let mut dates = Vec::<NaiveDate>::new();
    let mut values = Vec::<f64>::new();

    for record in reader.deserialize::<Record>() {
        let record: Record = record?;
        dates.push(record.date);
        values.push(record.value);
    }
    
    let values: Array1<f64> = Array1::from_vec(values);

    let alpha_28: f64 = alpha_from_span(28);
    let alpha_48: f64 = alpha_from_span(48);

    let mut start = Instant::now();
    let ema_28: Array1<f64> = exponential_moving_average(&values, alpha_28, false);
    let mut duration = start.elapsed();
    println!("Time elapsed in exponential_moving_average() is: {:?}", duration);

    start = Instant::now();
    let ema_48: Array1<f64> = exponential_moving_average(&values, alpha_48, false);
    duration = start.elapsed();
    println!("Time elapsed in exponential_moving_average() is: {:?}", duration);

    start = Instant::now();
    let sma_20: Array1<f64> = simple_moving_average(&values, 20);
    duration = start.elapsed();
    println!("Time elapsed in simple_moving_average() is: {:?}", duration);

    start = Instant::now();
    let heat: Array1<f64> = explicit_heat_smooth(&values, 50.0);
    duration = start.elapsed();
    println!("Time elapsed in explicit_heat_smooth() is: {:?}", duration);

    let dates_a: Array1<NaiveDate> = Array1::from_vec(dates);

    let trace_ema_28 = Scatter::from_array(dates_a.clone(), ema_28).name("EMA 28");
    let trace_ema_48 = Scatter::from_array(dates_a.clone(), ema_48).name("EMA 48");
    let trace_sma_20 = Scatter::from_array(dates_a.clone(), sma_20).name("SMA 20");
    let trace_heat = Scatter::from_array(dates_a.clone(), heat).name("Heat");
    let trace_raw = Scatter::from_array(dates_a, values).name("Raw");

    let mut plot = Plot::new();
    plot.add_trace(trace_ema_28);
    plot.add_trace(trace_ema_48);
    plot.add_trace(trace_sma_20);
    plot.add_trace(trace_heat);
    plot.add_trace(trace_raw);
    plot.show();

    Ok(())
}
