import pandas as pd

df = pd.DataFrame({'A': [0, 1, 2, 3, 4]})

print("adjust=True:", df.ewm(com=0.5, adjust=True).mean())

print("adjust=False:", df.ewm(com=0.5, adjust=False).mean())

print("rolling: ", df.rolling(2).mean())
print("rolling: ", df.rolling(4).mean())

df = pd.DataFrame({'A': [4, 3, 2, 1, 0]})

print("span=28, adjust=True:", df.ewm(span=28, adjust=True).mean())
print("span=28, adjust=False:", df.ewm(span=28, adjust=False).mean())
print("span=48, adjust=True:", df.ewm(span=48, adjust=True).mean())
print("span=48, adjust=False:", df.ewm(span=48, adjust=False).mean())